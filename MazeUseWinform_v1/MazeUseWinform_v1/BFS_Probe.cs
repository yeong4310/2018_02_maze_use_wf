﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeUseWinform_v1
{
    public class BFS_Probe : Probe
    {   //너비 우선 탐색
        public BFS_Probe(ReadMaze readmaze)
        {
            maze = readmaze;
            que = new Queue<Location2D>();
            path = new List<Location2D>();
            que.Enqueue(LocationCopy(1, 0));

            while (que.Count != 0)
            {
                Location2D here = que.Dequeue();
                path.Add(here);
                if (maze.location(here) == 3)
                {
                    maze.SaveToString("BFS for STRING.txt");
                    return;
                }
                else
                {
                    BFS_ChangeLocation(here);
                    if (ValidLoc(here) > 1)
                    {
                        if (isValidLoc(here.Row - 1, here.Col)) { que.Enqueue(LocationCopy(here.Row - 1, here.Col)); }
                        if (isValidLoc(here.Row + 1, here.Col)) { que.Enqueue(LocationCopy(here.Row + 1, here.Col)); }
                        if (isValidLoc(here.Row, here.Col - 1)) { que.Enqueue(LocationCopy(here.Row, here.Col - 1)); }
                        if (isValidLoc(here.Row, here.Col + 1)) { que.Enqueue(LocationCopy(here.Row, here.Col + 1)); }
                    }
                    else
                    {
                        if (isValidLoc(here.Row - 1, here.Col)) { que.Enqueue(LocationCopy(here.Row - 1, here.Col)); }
                        if (isValidLoc(here.Row + 1, here.Col)) { que.Enqueue(LocationCopy(here.Row + 1, here.Col)); }
                        if (isValidLoc(here.Row, here.Col - 1)) { que.Enqueue(LocationCopy(here.Row, here.Col - 1)); }
                        if (isValidLoc(here.Row, here.Col + 1)) { que.Enqueue(LocationCopy(here.Row, here.Col + 1)); }
                    }
                }
            }
        }
    }
}
