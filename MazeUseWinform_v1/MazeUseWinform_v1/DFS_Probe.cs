﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeUseWinform_v1
{
    public class DFS_Probe : Probe
    {   //깊이 우선 탐색
        public DFS_Probe(ReadMaze readmaze)
        {
            maze = readmaze;
            stack = new Stack<Location2D>();
            path = new List<Location2D>();
            stack.Push(LocationCopy(1, 0));

            while (stack.Count != 0)
            {
                Location2D here = stack.Pop();
                path.Add(here);
                if (maze.location(here) == 3)
                {
                    maze.SaveToString("DFS for String.txt");
                    return;
                }
                else
                {
                    DFS_ChangeLocation(here);

                    if(ValidLoc(here) > 1)
                    {
                        if (isValidLoc(here.Row - 1, here.Col)) { stack.Push(LocationCopy(here.Row - 1, here.Col)); }
                        if (isValidLoc(here.Row + 1, here.Col)) { stack.Push(LocationCopy(here.Row + 1, here.Col)); }
                        if (isValidLoc(here.Row, here.Col - 1)) { stack.Push(LocationCopy(here.Row, here.Col - 1)); }
                        if (isValidLoc(here.Row, here.Col + 1)) { stack.Push(LocationCopy(here.Row, here.Col + 1)); }
                    }
                    else
                    {
                        if (isValidLoc(here.Row - 1, here.Col)) { stack.Push(LocationCopy(here.Row - 1, here.Col)); }
                        if (isValidLoc(here.Row + 1, here.Col)) { stack.Push(LocationCopy(here.Row + 1, here.Col)); }
                        if (isValidLoc(here.Row, here.Col - 1)) { stack.Push(LocationCopy(here.Row, here.Col - 1)); }
                        if (isValidLoc(here.Row, here.Col + 1)) { stack.Push(LocationCopy(here.Row, here.Col + 1)); }
                    }
                }
            }

        }
    }
}
