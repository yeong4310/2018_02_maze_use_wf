﻿namespace MazeUseWinform_v1
{
    partial class Form1
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SizeMaze = new System.Windows.Forms.TextBox();
            this.btn_InitializeMaze = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.BFS = new System.Windows.Forms.Button();
            this.DFS = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(500, 450);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.SizeMaze);
            this.groupBox1.Controls.Add(this.btn_InitializeMaze);
            this.groupBox1.Location = new System.Drawing.Point(12, 468);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(213, 91);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "미로 만들기";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "미로의 크기";
            // 
            // SizeMaze
            // 
            this.SizeMaze.Location = new System.Drawing.Point(93, 26);
            this.SizeMaze.Name = "SizeMaze";
            this.SizeMaze.Size = new System.Drawing.Size(62, 21);
            this.SizeMaze.TabIndex = 1;
            // 
            // btn_InitializeMaze
            // 
            this.btn_InitializeMaze.Location = new System.Drawing.Point(109, 59);
            this.btn_InitializeMaze.Name = "btn_InitializeMaze";
            this.btn_InitializeMaze.Size = new System.Drawing.Size(75, 23);
            this.btn_InitializeMaze.TabIndex = 0;
            this.btn_InitializeMaze.Text = "미로 생성";
            this.btn_InitializeMaze.UseVisualStyleBackColor = true;
            this.btn_InitializeMaze.Click += new System.EventHandler(this.btn_InitializeMaze_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.BFS);
            this.groupBox2.Controls.Add(this.DFS);
            this.groupBox2.Location = new System.Drawing.Point(231, 468);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(111, 91);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "미로찾기";
            // 
            // BFS
            // 
            this.BFS.Location = new System.Drawing.Point(6, 54);
            this.BFS.Name = "BFS";
            this.BFS.Size = new System.Drawing.Size(75, 23);
            this.BFS.TabIndex = 5;
            this.BFS.Text = "너비우선";
            this.BFS.UseVisualStyleBackColor = true;
            this.BFS.Click += new System.EventHandler(this.BFS_Click);
            // 
            // DFS
            // 
            this.DFS.Location = new System.Drawing.Point(6, 24);
            this.DFS.Name = "DFS";
            this.DFS.Size = new System.Drawing.Size(75, 23);
            this.DFS.TabIndex = 4;
            this.DFS.Text = "깊이우선";
            this.DFS.UseVisualStyleBackColor = true;
            this.DFS.Click += new System.EventHandler(this.DFS_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(540, 567);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox SizeMaze;
        private System.Windows.Forms.Button btn_InitializeMaze;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button DFS;
        private System.Windows.Forms.Button BFS;
    }
}

