﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MazeUseWinform_v1
{
    public partial class Form1 : Form
    {
        MakeMaze mademaze;
        List<Location2D> searchPath;
        Graphics myG;
        Pen myPen;
        Timer timer;
        int[,] newMaze;
        int timerCount;
        int rctW, rctH, rctXLoc, rctYLoc;
        int mazeRow, mazeCol = 0;

        public Form1()
        {
            InitializeComponent();
            myG = this.pictureBox1.CreateGraphics();
            myPen = new Pen(Color.Black, 1);
        }

        private void SearchMaze()
        {
            MazeView();
            timerCount = 0;
            timer = new Timer
            {
                Interval = 1,
                Enabled = true
            };
            timer.Tick += searchMaze_Tick;
        }

        private void searchMaze_Tick(object sender, EventArgs e)
        {
            if(timerCount >= searchPath.Count)
            {
                timer.Enabled = false;
                MessageBox.Show("탐색 완료");
                timerCount = 0;
                return;
            }
            else
            {
                Rectangle path = new Rectangle(rctXLoc + rctW * searchPath[timerCount].Col,
                rctYLoc + rctH * searchPath[timerCount].Row, rctW, rctH);
                myG.FillRectangle(new SolidBrush(Color.Khaki), path);
                timerCount++;
            }            
        }

        public void MazeView()
        {
            myG.Clear(Color.White);
            int count = 0;
            ArrayList rct = new ArrayList();

            for (int y = 0; y < mazeRow; y++)
            {
                int yInitialLocation = rctYLoc + rctH * y;
                for (int x = 0; x < mazeCol; x++)
                {
                    int xInitialLocation = rctXLoc + rctW * x;
                    rct.Add(new Rectangle(xInitialLocation, yInitialLocation, rctW, rctH));
                }
            }
            foreach(Rectangle rectangle in rct)
            {
                if (newMaze[count / mazeCol, count % mazeCol] == 0)
                {
                    myG.DrawRectangle(myPen, rectangle);
                    count++;
                }
                else if(newMaze[count/mazeCol, count % mazeCol] == 1)
                {
                    myG.FillRectangle(new SolidBrush(Color.Black), rectangle);
                    count++;
                }
                else if(newMaze[count/mazeCol, count % mazeCol] == 2)
                {
                    myG.FillRectangle(new SolidBrush(Color.DarkRed), rectangle);
                    count++;
                }
                else if (newMaze[count / mazeCol, count % mazeCol] == 3)
                {
                    myG.FillRectangle(new SolidBrush(Color.Gray), rectangle);
                    count++;
                }
            }
        }

        private void btn_InitializeMaze_Click(object sender, EventArgs e)
        {
            if (SizeMaze.Text == "")
            {
                MessageBox.Show("값을 입력해주세요");
            }
            else
            {
                try
                {
                    mazeRow = int.Parse(SizeMaze.Text);
                    mazeCol = int.Parse(SizeMaze.Text);
                    MessageBox.Show($"{mazeRow} X {mazeCol}크기의 미로 생성");
                    rctH = this.pictureBox1.ClientSize.Height / (mazeRow + 2);      //사각형의 높이
                    rctW = this.pictureBox1.ClientSize.Width / (mazeCol + 2);       //사각형의 너비
                    if (rctH > rctW)
                    {
                        rctH = rctW;
                    }
                    else
                    {
                        rctW = rctH;
                    }
                    rctXLoc = (pictureBox1.ClientSize.Width - mazeCol * rctW) / 2;
                    rctYLoc = (pictureBox1.ClientSize.Height - mazeRow * rctH) / 2;
                    mademaze = new MakeMaze(mazeRow, mazeCol);  
                    newMaze = new int[mazeRow, mazeCol];
                    newMaze = mademaze.GetMaze;
                    for (int y = 0; y < mazeRow; y++)
                    {
                        for (int x = 0; x < mazeCol; x++)
                        {
                            newMaze[y, x] = mademaze.GetMaze[y, x];
                        }
                    }
                    mademaze.SaveToInt("MazeToInt.txt");
                    mademaze.SaveToString("MazeToString.txt");
                    MazeView();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void BFS_Click(object sender, EventArgs e)
        {
            BFS_Probe bfs = new BFS_Probe(new ReadMaze("MazeToInt.txt", mazeRow, mazeCol));
            searchPath = bfs.getPath();
            SearchMaze();
        }

        private void DFS_Click(object sender, EventArgs e)
        {
            DFS_Probe dfs = new DFS_Probe(new ReadMaze("MazeToInt.txt", mazeRow, mazeCol));
            searchPath = dfs.getPath();
            SearchMaze();
        }
    }
}
