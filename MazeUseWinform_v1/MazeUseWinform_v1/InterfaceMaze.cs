﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeUseWinform_v1
{
    interface InterfaceMaze
    {
        int location(int row, int col);
        int location(Location2D loc);
        void SaveToString(string filename);
        void SaveToInt(string filename);
    }
}
