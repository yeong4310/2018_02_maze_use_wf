﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeUseWinform_v1
{
    public class Location2D
    {
        public int Row { get; set; }        //y축
        public int Col { get; set; }        //x축

        public Location2D(int row, int col)
        {
            Row = row;      //y축
            Col = col;      //x축

        }
        public bool isNeighbor(Location2D p)
        {
            return (Col == p.Col && (Row == p.Row - 1 || Row == p.Row + 1)) || (Row == p.Row && (Col == p.Col - 1 || Col == p.Col + 1));
        }
    }
}
