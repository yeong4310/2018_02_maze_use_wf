﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace MazeUseWinform_v1
{
    public class MakeMaze : Maze
    {
        public MakeMaze(int row, int col) : base()   
        {   //생성자로 미로를 만듬
            Row = row;
            Col = col;
            maze = new int[Row, Col];
            for (int y = 0; y < Row; y++)
            {
                for (int x = 0; x < Col; x++)
                {
                    maze[y, x] = 1;
                }
            }
            maze[1, 0] = 2;
            maze[Row - 2, Col - 1] = 3;
            MazeCreate();
        }

        public void MazeCreate()
        {   //초기화 한 미로에 랜덤으로 길을 뚫는 함수
            Random random = new Random();
            int x = 1, y = 1;
            Dictionary<int, List<int>> map = new Dictionary<int, List<int>>(); //Dictionary에 value값으로 List를 담아 생성
            List<int> list = new List<int>();
            int cnt = 1;

            maze[1, 1] = 0;
            //출구(row-2, col-1)에 도달할 때 까지 반복
            while (!(y == maze.GetLength(0) - 2 && x == maze.GetLength(0) - 2))
            {
                int randCourse = (int)(random.NextDouble() * 4 + 1);    //방향을 랜덤으로 선택
                int routeLen = (int)(random.NextDouble() * (maze.GetLength(0) / 5) + 1); //길이를 크기에 비례하여 랜덤으로 선택

                list.Insert(0, y);
                list.Insert(1, x);
                list.Insert(2, randCourse);

                map.Add(cnt, list);     //key와 value로 count(현재 1)와 list를 담아준다.
                int getRandCourse = map[cnt++][2];

                if (randCourse == 1 && getRandCourse != 3)
                {   //랜덤으로 정해진 방향이 위쪽이고, 직전에 갔던 방향이 아래쪽이 아닐때
                    for (int a = 0; a < routeLen; a++)
                    {
                        if (y < 2)
                            break;
                        y--;
                        maze[y, x] = 0;
                    }
                }
                else if (randCourse == 4 && getRandCourse != 2)
                {   //랜덤으로 정해진 방향이 왼쪽이고, 직전에 갔던 방향이 오른쪽이 아닐때
                    for (int a = 0; a < routeLen; a++)
                    {
                        if (x < 2)
                            break;
                        x--;
                        maze[y, x] = 0;
                    }
                }
                else if (randCourse == 3 && getRandCourse != 1)
                {   //랜덤으로 정해진 방향이 아래쪽이고, 직전에 갔던 방향이 위쪽이 아닐때
                    for (int a = 0; a < routeLen; a++)
                    {
                        if (y >= maze.GetLength(0) - 2)
                            break;
                        y++;
                        maze[y, x] = 0;
                    }
                }
                else if (randCourse == 2 && getRandCourse != 4)
                {   //랜덤으로 정해진 방향이 오른쪽이고, 직전에 갔던 방향이 왼쪽이 아닐때
                    for (int a = 0; a < routeLen; a++)
                    {
                        if (x >= maze.GetLength(0) - 2)
                            break;
                        x++;
                        maze[y, x] = 0;
                    }
                }
                else if (cnt > routeLen * 4)
                {   //랜덤으로 n번째 전으로 돌아감
                    cnt -= (routeLen - 1);
                    y = map[cnt][0];
                    x = map[cnt][1];
                }
            }
        }
    }
}
