﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

//0 : 길
//1 : 벽
//2 : 시작
//3 : 끝
//4 : 최단거리 지나온길
//5 : 깊이우선탐색의 지나온길
//6 : 너비우선탐색의 지나온길
namespace MazeUseWinform_v1
{
    public class Maze : InterfaceMaze
    {
        public int[,] maze;              //미로를 저장할 2차원배열
        public int Row { get; set; }        //y좌표
        public int Col { get; set; }        //x좌표
        public int[,] GetMaze
        {
            get { return maze; }
        }
        public int location(int row, int col)
        {
            return maze[row, col];
        }

        public int location(Location2D loc)
        {
            return maze[loc.Row, loc.Col];
        }

        public void BFS_SetLocation(Location2D loc, string type)
        {
            if (type == "BFSpath")       //너비우선탐색 지나온길
            {
                maze[loc.Row, loc.Col] = 6;
            }
        }
        public void DFS_SetLocation(Location2D loc, string type)
        {
            if (type == "DFSpath")       //깊이우선탐색 지나온길
            {
                maze[loc.Row, loc.Col] = 5;
            }
        }
        public void Dijkstra_SetLocation(Location2D loc, string type)
        {
            if (type == "Dijkstrapath")       //최단거리탐색 지나온길
            {
                maze[loc.Row, loc.Col] = 4;
            }
        }

        public void SaveToString(string filename)
        {
            StreamWriter writer = null;
            try
            {
                writer = new StreamWriter(new FileStream(filename, FileMode.Create));
                for (int y = 0; y < Row; y++)
                {
                    for (int x = 0; x < Col; x++)
                    {
                        if (maze[y, x] == 0)
                        {
                            writer.Write("□");
                        }
                        else if (maze[y, x] == 1)
                        {
                            writer.Write("■");
                        }
                        else if (maze[y, x] == 2)
                        {
                            writer.Write("♣");
                        }
                        else if (maze[y, x] == 3)
                        {
                            writer.Write("●");
                        }
                        else if (maze[y, x] == 4)
                        {
                            writer.Write("※");
                        }
                        else if (maze[y, x] == 5)
                        {
                            writer.Write("☆");
                        }
                        else if (maze[y, x] == 6)
                        {
                            writer.Write("@");
                        }
                    }
                    writer.WriteLine();
                }
            }
            catch (IOException e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                try
                {
                    writer.Close();
                }
                catch (IOException e)
                {
                    Console.WriteLine(e);
                }
            }
        }

        public void SaveToInt(string filename)
        {
            StreamWriter writer = null;
            try
            {
                writer = new StreamWriter(new FileStream(filename, FileMode.Create));
                for (int y = 0; y < Row; y++)
                {
                    for (int x = 0; x < Col; x++)
                    {
                        writer.Write(maze[y, x]);
                    }
                    writer.WriteLine();
                }
            }
            catch (IOException e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                try
                {
                    writer.Close();
                }
                catch (IOException e)
                {
                    Console.WriteLine(e);
                }
            }
        }

    }
}