﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MazeUseWinform_v1
{
    public class Probe
    {
        protected Stack<Location2D> stack;
        protected Queue<Location2D> que;
        protected List<Location2D> path;
        protected Maze maze;

        public List<Location2D> getPath()
        {
            return path;
        }

        public Location2D LocationCopy(int row, int col)
        {
            Location2D temp = new Location2D(row, col);
            return temp;
        }

        public void BFS_ChangeLocation(Location2D loc)
        {//너비우선탐색시 경로판단
            if (ValidLoc(loc) >= 1)
            {
                maze.BFS_SetLocation(loc, "BFSpath");
            }
        }

        public void DFS_ChangeLocation(Location2D loc)
        {//깊이우선탐색시 경로판단
            if (ValidLoc(loc) >= 1)
            {
                maze.DFS_SetLocation(loc, "DFSpath");
            }
        }

        public void Dijkstra_ChangeLocation(Location2D loc)
        {//최단거리탐색시 경로판단
            if (ValidLoc(loc) >= 1)
            {
                maze.Dijkstra_SetLocation(loc, "Dijkstrapath");
            }
        }

        public bool isValidLoc(int row, int col)    
        {   //진행이 가능한지 판단
            if (row < 0 || col < 0 || row >= maze.Row || col >= maze.Col)
            {
                return false;
            }
            else
            {
                if (maze.location(row, col) == 0 || maze.location(row, col) == 3)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public int ValidLoc(Location2D loc)
        {    //해당 좌표에서 이동가능한 경로의 갯수 판단
            int PossiblePath = 0;
            if (isValidLoc(loc.Row - 1, loc.Col))
            {
                PossiblePath++;
            }
            if (isValidLoc(loc.Row + 1, loc.Col))
            {
                PossiblePath++;
            }
            if (isValidLoc(loc.Row, loc.Col - 1))
            {
                PossiblePath++;
            }
            if (isValidLoc(loc.Row, loc.Col + 1))
            {
                PossiblePath++;
            }
            return PossiblePath;
        }
    }
}

