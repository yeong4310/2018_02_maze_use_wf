﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections;

namespace MazeUseWinform_v1
{
    public class ReadMaze : Maze
    {
        public ReadMaze(string filename, int row, int col)
        {
            StreamReader reader = null;
            try
            {
                reader = new StreamReader(filename);
                if (reader == null)
                {
                    throw new Exception("파일이 존재하지 않습니다");
                }
                else
                {
                    Row = row;
                    Col = col;
                    maze = new int[row, col];
                    string txtLine = "";
                    int entity = 0;

                    ArrayList list = new ArrayList();
                    while (entity < Row)
                    {
                        txtLine = reader.ReadLine();            //텍스트를 한줄씩 읽어옴
                        if (txtLine != null)
                        {
                            list.Add(txtLine);
                            char[] line = txtLine.ToArray();        //한줄씩 읽어온 텍스트를 char단위로 자름
                            for (int x = 0; x < col; x++)
                            {
                                if (line[x] == '0')
                                {
                                    maze[entity, x] = 0;
                                }
                                else if (line[x] == '1')
                                {
                                    maze[entity, x] = 1;
                                }
                                else if (line[x] == '2')
                                {
                                    maze[entity, x] = 2;
                                }
                                else if (line[x] == '3')
                                {
                                    maze[entity, x] = 3;
                                }
                            }
                            entity++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            finally
            {
                try
                {
                    reader.Close();
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }
        }
    }
}
